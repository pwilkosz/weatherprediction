import datetime
import pandas as pd
import numpy as np
import re
from geopy.distance import vincenty
import sys
class wStation(object):
    '''
    wStation Class:

    Contains information about weather station: Name, latitude, longitude, altitude, wmo index.
    It also has historical weather data like temperature, wind, pressure
    '''
    def __init__(self, name, latitude, longitude, altitude, wmo):
        self.Name = name
        self.Latitude = latitude
        self.Longitude = longitude
        self.Altitude = altitude
        self.WmoIndex = wmo
        self.df = pd.DataFrame()
    def importData(self, days, endDay):
        #here will be the code from getData.py
        pass
    def __str__(self):
        return str(self.__dict__)
    def calculateDistance(self, station):
        '''
        Calculates distance between two stations
        :param station: wStation object
        :return: int value in kilometers
        '''
        regexlat = re.compile(r'(\d\d)-(\d\d).*([N|S])')
        regexlong = re.compile(r'(\d\d\d)-(\d\d).*([W|E])')
        lat = dict()
        long = dict()
        #latitude
        try:
            tmpstr = regexlat.search(self.Latitude)
            lat["A"] = (float(tmpstr.group(1)) + (float(tmpstr.group(2))*(100.0/60.0))/100.0)*(1 if tmpstr.group(2) == 'N' else -1)
            tmpstr = regexlat.search(station.Latitude)
            lat["B"] = (float(tmpstr.group(1)) + (float(tmpstr.group(2)) * (100.0 / 60.0)) / 100.0)*(1 if tmpstr.group(2) == 'N' else -1)
            #longtitude
            tmpstr = regexlong.search(self.Longitude)
            long["A"] = (float(tmpstr.group(1)) + (float(tmpstr.group(2)) * (100.0 / 60.0)) / 100.0)*(1 if tmpstr.group(2) == 'E' else -1)
            tmpstr = regexlong.search(station.Longitude)
            long["B"] = (float(tmpstr.group(1)) + (float(tmpstr.group(2)) * (100.0 / 60.0)) / 100.0)*(1 if tmpstr.group(2) == 'E' else -1)
        except AttributeError:
            print(self)
            print(station)
            sys.exit("program stopped working")

        #calculate distance
        '''
        what we need
        geopy!!!
        '''
        dist = vincenty((lat["A"], long["A"]),(lat["B"], long["B"])).kilometers
        return dist