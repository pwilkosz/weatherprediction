import pandas as pd
from functools import reduce
from matplotlib import pyplot as plt
'''
defines particular weather parameter like temperature, pressure etc

'''
# TODO: top(n) - top n items with city name and value - nlargest
# TODO: topCorrelatedCities(n, city) - top n cities best correlated with the given city - nlargest
# TODO: correlation(city) - plots correlation graph (scatter plot)
# TODO: plot value over time
class weather(object):
    def __init__(self):
        pass
    def correlation(self, city, dir):
        '''
        Calculates correlation between cities
        Calculates relationships between distance and correlation coefficient
        :param city: String
        :param dir: String - directory where .csv file will be saved
        :return: DataFrame with correlation and distance
        '''
        corrdf = self.wstations.corr()
        corrdf.to_csv('../csv/corr.csv')
        ref = self.station_tmp[city]
        distance_dict = dict((i.Name, i.calculateDistance(ref)) for i in self.station_list)
        distancepd = pd.DataFrame.from_dict(pd.Series(distance_dict))
        distancepd.columns = ["city"]
        distancepd["corr"] = corrdf[city]
        if dir != "":
            distancepd.to_csv("../csv/correlations/{}".format(dir))
        return distancepd
        # print(distancepd["city"])
        # plt.scatter(distancepd["city"], distancepd["corr"])
        # plt.show()

    def stats(self, city):
        '''
        shows stats regarding particular parameter with correlation scatter plot
        and cities with largest values
        examples:
        graph1: Temperature in Wroclaw and mean value
        graph2: correlation graph
        graph3: bar chart with cities with highest temperature
        :param city: city for which we'll be printing the graph
        :return: void
        '''
        # Temperature with the mean
        ax1 = plt.subplot2grid((2,2), (0,0), rowspan=1, colspan=2)
        ax2 = plt.subplot2grid((2,2), (1,0), rowspan=1, colspan=1)
        ax3 = plt.subplot2grid((2,2), (1,1), rowspan=1, colspan=1)
        ax1.plot(self.wstations[city])
        ax1.plot(self.wstations.mean(axis=1))
        corr_tmp = self.correlation(city, "")
        ax2.scatter(corr_tmp["city"], corr_tmp["corr"])
        top_tmp = self.top(10)
        ax3.bar(range(10), top_tmp.values, align='center')
        ax3.set_xticklabels(top_tmp.index)
        #df1 = self.correlation(city, "")
        #df2 = self.wstations[city]

        plt.show()

    def top(self, n):
        '''

        :param n: number of stations
        :return: dataframe.nlargest
        '''

        return self.wstations.mean(axis=0).nlargest(n)



class temperature(weather):
    def __init__(self, dfObj):
        self.station_tmp = dfObj
        self.station_list = self.station_tmp.values()

        # merges temparature values
        self.wstations = reduce(lambda x, y: pd.merge(x, y, on=["DateTime"]),
                           (i.df[["DateTime", "Temp_{}".format(i.Name)]] for i in self.station_list))
        self.wstations.columns = ["DateTime"] + [item for item in self.station_tmp.keys()]
        self.wstations = self.wstations.convert_objects(convert_numeric=True)
        cityy = "Sniezka"
        self.wstations[cityy].plot()
        self.wstations.mean(axis=1).plot()
        plt.legend().remove()
        plt.show()

class pressure(weather):
    def __init__(self, dfObj):
        self.station_tmp = dfObj
        self.station_list = self.station_tmp.values()

        # merges temparature values
        self.wstations = reduce(lambda x, y: pd.merge(x, y, on=["DateTime"]),
                           (i.df[["DateTime", "Pressure_{}".format(i.Name)]] for i in self.station_list))
        self.wstations.columns = ["DateTime"] + [item for item in self.station_tmp.keys()]
        #self.wstations.convert_objects(convert_numeric=True)

class windSpeed(weather):
    def __init__(self, dfObj):
        self.station_tmp = dfObj
        self.station_list = self.station_tmp.values()

        # merges temparature values
        self.wstations = reduce(lambda x, y: pd.merge(x, y, on=["DateTime"]),
                           (i.df[["DateTime", "WindSpeed_{}".format(i.Name)]] for i in self.station_list))
        self.wstations.columns = ["DateTime"] + [item for item in self.station_tmp.keys()]
        #self.wstations.convert_objects(convert_numeric=True)