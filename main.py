from src.getData import grabStationData
import pandas as pd
import pickle
from functools import reduce
import matplotlib.pyplot as plt
from matplotlib import style
from src.weather import temperature, pressure, windSpeed
from src.utils import saveData



style.use('fivethirtyeight')
# grabStationData('Poland', '4', '2016', '11', '27', '12')

station_pickle = open('weatherStations.pickle', 'rb')
station_tmp = (pickle.load(station_pickle))
station_list = station_tmp.values()


tmp = temperature(station_tmp)
tmp.correlation("Wielun", "TempWielun.csv")
print(tmp.top(10).index)
tmp.stats("Siedlce")

# station1 = station_list[1].df[["Time", "Date", "Temp"]]
# station2 = station_list[2].df[["Time", "Date", "Temp"]]
# tmp = pd.merge(station1, station2, on=["Time", "Date"])
# tmp["Temp_x"] = tmp["Temp_x"].astype(float)
# tmp["Temp_y"] = tmp["Temp_y"].astype(float)
# fig = plt.figure()
# ax1 = plt.subplot2grid((2,1), (0,0))
# ax2 = plt.subplot2grid((2,1), (1,0), sharex=ax1)
# tcorr = pd.rolling_corr(tmp["Temp_x"], tmp["Temp_y"], 12)
# tmp["Temp_x"].plot(ax=ax1, label = "a")
# tmp["Temp_y"].plot(ax=ax1, label = "b")
# print(tcorr)
# ax1.legend(loc=4)
# tcorr.plot(ax=ax2)
# plt.show()

