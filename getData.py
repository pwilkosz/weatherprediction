import pickle
import pandas as pd
import numpy as np
from src.wStation import wStation
#now only for Poland
def grabStationData(contry, ndays, year, month, day, hour):
    '''
        Data to fetch
        1. WMO index
        2. City name
        3. Latitude
        4. Longtitude
        5. Altitude
        Columns to fetch
        1.Date/Time
        2.T (C)
        3.Wind (direction and speed)
        4.Pressure
        5.Precipation
        Each column needs to be verified
        --------------------------------------
        Regular Expressions matching for particular columns
        Date - "\d{2}//\d{2}//\d{4}"
    '''
    print('fetching stations\' data', end="...")
    station_list = pd.read_html(
        'http://www.ogimet.com/display_stations.php?lang=en&tipo=AND&isyn=&oaci=&nombre=&estado=Poland&Send=Send')

    wmo_list = pd.DataFrame(station_list[0])
    wmo_list = wmo_list.iloc[:, [0, 2, 4, 5, 6]]

    wmo_list1 = wmo_list.where(wmo_list.iloc[:, 0].str.match('\d{5}'), other=np.nan)
    wmo_list1.dropna(inplace=True)

    wmo_list1.columns = ["WMO", "Name", "Latitude", "Longitude", "Altitude"]
    station_list = [
        wStation(name=row["Name"], latitude=row["Latitude"], longitude=row["Longitude"], altitude=row["Altitude"],
                 wmo=row["WMO"]) for index, row in wmo_list1.iterrows()]


    # here need to put weather data for each station (50 days)
    print("done")
    print([i.Name for i in station_list])
    for station in reversed(station_list):
        print('getting data for {} - {}'.format(station.Name, station.WmoIndex), end="...")
        #grab data for each station
        station_html = pd.read_html(
            'http://www.ogimet.com/cgi-bin/gsynres?ind={}&lang=en&decoded=yes&ndays={}&ano={}&mes={}&day={}&hora={}'.format(
            station.WmoIndex, ndays, year, month, day, hour))


        try:
            station_df = pd.DataFrame(station_html[0])

            # TODO: convert_numeric, convert_datetime
            station.df["DateTime"] = station_df.iloc[:, 0] + ' ' + station_df.iloc[:, 1]
            station.df["Temp_{}".format(station.Name)] = pd.to_numeric(station_df.iloc[:, 2], errors='ignore')
            station.df["WindDirection_{}".format(station.Name)] = pd.to_numeric(station_df.iloc[:, 6], errors='ignore')
            station.df["WindSpeed_{}".format(station.Name)] = pd.to_numeric(station_df.iloc[:, 7], errors='ignore')
            station.df["Pressure_{}".format(station.Name)] = pd.to_numeric(station_df.iloc[:, 9], errors='ignore')


            #station.df = station_df.iloc[:, [0, 1, 2, 6, 7, 9]]

        except IndexError:
            station_list.remove(station)
            #print('{} removed'.format(station.Name))
            continue

        #station.df.columns = ["Date", "Time", "Temp_{}".format(station.Name), "WindDirection_{}".format(station.Name), \
         #                     "WindSpeed_{}".format(station.Name), "Pressure_{}".format(station.Name)]

        station.df.set_index(["DateTime"])

        station.df = station.df.where(station.df["DateTime"].str.match('\d{2}\/\d{2}\/\d{4}'), other=np.nan)

        station.df.dropna(inplace=True)
        station.df["DateTime"] = pd.to_datetime(station.df["DateTime"], format='%m/%d/%Y %H:%M')
        #print(station.df.head(20))
        print("done")
        #print(station.df)
    station_names = [i.Name for i in station_list]

    station_data = dict(zip(station_names, station_list))
    station_pickle = open('weatherStations.pickle', 'wb')
    pickle.dump(station_data, station_pickle)
    station_pickle.close()





